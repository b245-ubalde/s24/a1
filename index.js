// Getting the value of a cube

let getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);


// Printing the address
let address = [12, "Kentucky", "Mahabang Kahoy", "Indang", "Cavite", 4114];

let [houseNumber, street, barangay, municipality, province, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${barangay}, ${municipality}, ${province}, ${zipCode}.`);

// Printing a message about an animal

let animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurements: "20 ft 3 in"
}

let {name, species, weight, measurements} = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurements}`);



// Printing an array of numbers

let arrayNumber = [1, 2, 3, 4, 5];

arrayNumber.forEach((num) => console.log(num));



// Reduce number

let reduceNumber = arrayNumber.reduce((a, b) => a+b);
console.log(reduceNumber);


// Class Dog

class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let Dog = new dog("Frankie", 5, "Miniature Dachshund");
console.log(Dog);